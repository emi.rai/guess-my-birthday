from random import randint

name = input("Hi! What is your name? ")

low = 1924
high = 2004

month_day = {
    1: 31,
    2: 28,
    3: 31,
    4: 30,
    5: 31,
    6: 30,
    7: 31,
    8: 31,
    9: 30,
    10: 31,
    11: 30,
    12: 31
}

month_written = {
    1: 'January',
    2: 'February',
    3: 'March',
    4: 'April',
    5: 'May',
    6: 'June',
    7: 'July',
    8: 'August',
    9: 'September',
    10: 'October',
    11: 'November',
    12: 'December'
}

for i in range(5):
    guess_m = randint(1,12)
    guess_yyyy = randint(low, high)
    guess_d = randint(1, month_day.get(guess_m))
    guess_n = i + 1
    print("Guess ",guess_n,":",name,", were you born on " + month_written[guess_m] + " " + str(guess_d) + " , " + str(guess_yyyy) +"?") 
    response = input("yes or no? ")
    if response == "yes":
        print("I knew it!")
        break
    elif i < 4:
        helper = input("earlier or later? ")
        if helper == "earlier":
            high = guess_yyyy - 1
        elif helper == "later":
            low = guess_yyyy + 1
    else:
        print("I have other things to do. Good bye.")

    # Can use commas to print strings and ints
